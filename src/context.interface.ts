import { Context as ContextTelegraf } from 'telegraf';

export interface Context extends ContextTelegraf {
  session: {
    awaitingKey?: boolean;
    type?: 'MeetingCurator' | 'InfoForYou' | 'ScheduledEvents' | 'SupportMeasures' | 'RequestWithCurator' | 'code';
  };
}
