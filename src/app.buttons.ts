import { Markup } from 'telegraf';

export function actionButtons() {
  return Markup.keyboard(
    [
      Markup.button.callback('Запланированные мероприятия', 'ScheduledEvents'),
      Markup.button.callback('Меры поддержки', 'SupportMeasures'),
      Markup.button.callback('Информация для вас', 'InfoForYou'),
      Markup.button.callback('Даты встречи с куратором', 'MeetingCurator'),
      Markup.button.callback(
        'Запросить связь с куратором',
        'RequestWithCurator'
      ),
    ],
    {
      columns: 2,
    }
  );
}
