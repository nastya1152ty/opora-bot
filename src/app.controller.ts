import { Ctx, Hears, InjectBot, On, Start, Update } from 'nestjs-telegraf';
import { Markup, Telegraf } from 'telegraf';
import { actionButtons } from './app.buttons';
import { Context } from './context.interface';
import axios from 'axios';
import { format } from 'date-fns';

@Update()
export class AppController {
  constructor(@InjectBot() private readonly bot: Telegraf<Context>) {}

  @Start()
  async startCommand(ctx: Context) {
    await ctx.reply('Укажите уникальный ключ');
    ctx.session.awaitingKey = true;
  }

  @Hears('Меры поддержки')
  async getSupportMeasures(@Ctx() ctx: Context) {
    ctx.session.type = 'SupportMeasures';
    await ctx.reply('SupportMeasures');
    try {
      const response = await axios.post(process.env.DATABASE_USER, {
        query: `{
          getsSupportMeasures{
            value
          }
        }`,
      });
      const supportMeasures = response.data.data.getsSupportMeasures;
      supportMeasures.forEach((support) => {
        ctx.reply(support.value);
      });
    } catch (error) {
      console.error('Error fetching curator:', error);
      await ctx.reply('Произошла ошибка при получении данных.');
    }
  }

  @Hears('Запланированные мероприятия')
  async getScheduledEvents(@Ctx() ctx: Context) {
    ctx.session.type = 'ScheduledEvents';
    await ctx.reply('ScheduledEvents');
    try {
      const response = await axios.post(process.env.DATABASE_USER, {
        query: `
        query{
          getEventsWithCitizenStatus(user_bot_id: "${ctx.from.id}"){
            id
            name
            description
            dateStart
            dateEnd
            citizenOnEvent{
              isAttending
            }
          }
        }
        `,
      });

      const events = response.data.data.getEventsWithCitizenStatus;
      if (events.length === 0) {
        await ctx.reply('Нет запланированных мероприятий.');
      } else {
        events.forEach((event) => {
          const { id, name, description, dateStart, dateEnd } = event;
          const formattedDateStart = format(
            new Date(dateStart),
            'yyyy-MM-dd HH:mm'
          );
          const formattedDateEnd = format(
            new Date(dateEnd),
            'yyyy-MM-dd HH:mm'
          );

          let formattedData = `Дата начала: ${formattedDateStart}`;
          if (formattedDateEnd) {
            formattedData += `\nДата окончания: ${formattedDateEnd}`;
          }
          const attendingStatuses = event.citizenOnEvent.map(
            (c) => c.isAttending
          );
          const attendingStatus = attendingStatuses.some(
            (status) => status === true
          )
            ? 'Буду присутствовать'
            : 'Присутствие неизвестно';
          const eventMessage = `${name}\n${description}\n${formattedData}\n${attendingStatus}`;

          const keyboard = Markup.inlineKeyboard([
            Markup.button.callback('Пойду', `attend_${id}_true`),
            Markup.button.callback('Не пойду', `attend_${id}_false`),
          ]);
          ctx.reply(eventMessage, keyboard);
        });
      }
      this.bot.action(/attend_(\d+)_(true|false)/, async (ctx) => {
        const eventId = parseInt(ctx.match[1]);
        const isAttending = ctx.match[2] === 'true';
        try {
          await this.updateAttendingStatus(ctx.from.id, eventId, isAttending);
          const statusMessage = isAttending
            ? 'Вы записались на мероприятие.'
            : 'Вы отказались от участия в мероприятии.';
          await ctx.reply(statusMessage);
        } catch (error) {
          console.log(error);
          await ctx.reply('Произошла ошибка при обновлении статуса посещения.');
        }
      });
    } catch (error) {
      console.log(error);
    }
  }
  async updateAttendingStatus(
    userId: number,
    eventId: number,
    isAttending: boolean
  ) {
    try {
      const response = await axios.post(process.env.DATABASE_USER, {
        query: `
        mutation {
          isAttending(user_bot_id: "${userId}", eventId: ${eventId}, isAttending: ${isAttending}) {
            isAttending
          }
        }
      `,
      });
      return response.data;
    } catch (error) {
      console.error('Error updating attending status:', error);
      throw error;
    }
  }

  @Hears('Информация для вас')
  async getInfoForYou(@Ctx() ctx: Context) {
    ctx.session.type = 'InfoForYou';
    await ctx.reply('InfoForYou');

    try {
      const response = await axios.post(process.env.DATABASE_USER, {
        query: `{
        getCitizen(botId: "${ctx.from.id}") {
          curator {
            first_name
            last_name
            second_name
            phone
            email
          }
        }
      }`,
      });

      const curator = response.data.data.getCitizen.curator;
      if (curator) {
        const { first_name, last_name, second_name, phone, email } = curator;
        const formattedData = `ФИО: ${first_name} ${last_name} ${second_name}\nТелефон: ${phone}\nEmail: ${email}`;
        await ctx.reply(formattedData);
      } else {
        await ctx.reply('Куратор не найден.');
      }
    } catch (error) {
      console.error('Error fetching curator:', error);
      await ctx.reply('Произошла ошибка при получении данных.');
    }
  }

  @Hears('Даты встречи с куратором')
  async getMeetingCurator(@Ctx() ctx: Context) {
    ctx.session.type = 'MeetingCurator';
    await ctx.reply('Даты встречи с куратором');
    try {
      const response = await axios.post(process.env.DATABASE_USER, {
        query: `
          query {
            getActivitys(lastThirty: true, user_bot_id: "${ctx.from.id}") {
              id
              request_date
              confirm_date
              end_date
            }
          }
        `,
      });

      const activities = response.data.data.getActivitys;
      activities.forEach((activity) => {
        const { request_date, confirm_date, end_date, id } = activity;
        const formattedRequestDate = format(
          new Date(request_date),
          'yyyy-MM-dd HH:mm'
        );
        const formattedConfirmDate = confirm_date
          ? format(new Date(confirm_date), 'yyyy-MM-dd HH:mm')
          : null;
        const formattedEndDate = end_date
          ? format(new Date(end_date), 'yyyy-MM-dd HH:mm')
          : null;

        let formattedData = `Дата отправки: ${formattedRequestDate}`;
        if (formattedConfirmDate) {
          formattedData += `\nДата принятия: ${formattedConfirmDate}`;
        }
        if (formattedEndDate) {
          formattedData += `\nДата окончания: ${formattedEndDate}`;
        }
        const keyboard = Markup.inlineKeyboard([
          Markup.button.callback('1', `rate_${id}_1`),
          Markup.button.callback('2', `rate_${id}_2`),
          Markup.button.callback('3', `rate_${id}_3`),
          Markup.button.callback('4', `rate_${id}_4`),
          Markup.button.callback('5', `rate_${id}_5`),
        ]);

        ctx.reply(formattedData, keyboard);
      });
      this.bot.action(/rate_\d+_\d/, async (ctx) => {
        const [_, activityId, rating] = ctx.match[0].split('_');
        try {
          const currentReaction = await this.getActivityReaction(
            parseInt(activityId)
          );
          if (currentReaction === parseInt(rating)) {
            await ctx.reply('Вы уже поставили эту оценку.');
            return;
          }
          await await this.createReaction(
            parseInt(activityId),
            parseInt(rating)
          );
          await ctx.reply('Спасибо за вашу оценку!');
        } catch (error) {
          await ctx.reply(
            'Произошла ошибка при сохранении вашей оценки. Пожалуйста, попробуйте снова позже.'
          );
        }
      });
    } catch (error) {
      console.error('Error getting activities:', error);
      await ctx.reply('Произошла ошибка при получении активностей');
    }
  }
  async createReaction(activityId: number, reaction: number) {
    try {
      const response = await axios.post(process.env.DATABASE_USER, {
        query: `
          mutation {
            createReaction(id: ${activityId}, reaction: ${reaction}) {
              id
              reaction
              support_measureId
            }
          }
        `,
      });

      const createdReaction = response.data.data.createReaction;
      console.log('Created reaction:', createdReaction);
      return createdReaction;
    } catch (error) {
      console.error('Error creating reaction:', error);
      throw error;
    }
  }
  async getActivityReaction(activityId: number) {
    try {
      const response = await axios.post(process.env.DATABASE_USER, {
        query: `
          query {
            getActivity(id: ${activityId}) {
              reaction
            }
          }
        `,
      });

      const activity = response.data.data.getActivity;
      return activity ? activity.reaction : null;
    } catch (error) {
      console.error('Error fetching activity reaction:', error);
      throw error;
    }
  }

  @Hears('Запросить связь с куратором')
  async getRequestWithCurator(@Ctx() ctx: Context) {
    ctx.session.type = 'RequestWithCurator';

    try {
      const response = await axios.post(process.env.DATABASE_USER, {
        query: `
          mutation {
            createActivity(user_bot_id: "${ctx.from.id}") {
              id
              curatorId
              end_date
            }
          }
        `,
      });
      await ctx.reply('Зпрос отправлен. Куратор с вами скороо свяжется.');
    } catch (error) {
      console.error('Error creating activity:', error);
    }
  }

  @On('text')
  async handleText(@Ctx() ctx: Context) {
    let messageText: string | undefined;
    if ('text' in ctx.message) {
      messageText = ctx.message.text;
    } else if ('caption' in ctx.message) {
      messageText = ctx.message.caption;
    }
    if (ctx.session.awaitingKey) {
      if (messageText && messageText.trim().length > 0) {
        const userId = ctx.from.id;
        const code = messageText.trim();
        try {
          const response = await axios.post(process.env.DATABASE_USER, {
            query: `
              mutation {
                chatBotAuth(code: "${code}", user_bot_id: "${userId}") {
                  id
                  code
                  user_bot_id
                  first_name
                }
              }
            `,
          });
          if (response.data.data && response.data.data.chatBotAuth) {
            ctx.session.awaitingKey = false;
            ctx.session.type = null;
            await ctx.reply('Выберите действие:', actionButtons());
          } else {
            await ctx.reply(
              'Ключ не правильный. Введите ваш уникальный ключ еще раз'
            );
          }
        } catch (e) {
          console.log(e);
        }
      }
    }
  }
}
